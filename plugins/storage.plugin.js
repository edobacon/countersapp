export default function ({ store }) {
  const counters = localStorage.getItem('counters')
  counters && store.commit('counter/setCounters', { counters: JSON.parse(counters) })
  const filters = sessionStorage.getItem('filters')
  if (filters) {
    const f = JSON.parse(filters)
    store.commit('counter/setFilter', { filterName: f.name, filterMin: f.min, filterMax: f.max })
  }
}
