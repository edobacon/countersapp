import { saveToLocalStorage, saveToSessionStorage } from "../utils"

export const state = () => ({
  limits: {
    maxCounters: 20,
    minCount: 0,
    maxCount: 20,
  },
  order: {
    by: 'name',
    asc: true,
  },
  filter: {
    name: '',
    min: 0,
    max: 20
  },
  counters: [],
})

export const mutations = {
  setCounters(state, { counters }) {
    state.counters = counters
  },
  addCounter(state, { name }) {
    state.counters.length < state.limits.maxCounters &&
      state.counters.push({
        id: Date.now(),
        name,
        value: 0,
      })
    saveToLocalStorage('counters', state.counters)
  },
  incrementCounter(state, { id }) {
    const counter = state.counters.find((counter) => counter.id === id)
    counter.value < state.limits.maxCount && counter.value++
    saveToLocalStorage('counters', state.counters)
  },
  decrementCounter(state, { id }) {
    const counter = state.counters.find((counter) => counter.id === id)
    counter.value > state.limits.minCount && counter.value--
    saveToLocalStorage('counters', state.counters)
  },
  removeCounter(state, { id }) {
    state.counters = state.counters.filter((counter) => counter.id !== id)
    saveToLocalStorage('counters', state.counters)
  },
  setFilter(state, { filterName, filterMin, filterMax }) {
    state.filter.name = filterName
    state.filter.min = filterMin < state.limits.minCount ? state.limits.minCount : filterMin
    state.filter.max = filterMax > state.limits.maxCount ? state.limits.maxCount : filterMax
  },
  setOrderAndFilter(state, { orderName, orderType, filterName, filterMin, filterMax }) {
    state.order.by = orderName
    state.order.asc = orderType=== 'asc'
    state.filter.name = filterName
    state.filter.min = filterMin === '' || filterMin < state.limits.minCount ? state.limits.minCount : filterMin
    state.filter.max = filterMax === '' || filterMax > state.limits.maxCount ? state.limits.maxCount : filterMax
    saveToSessionStorage('filters', state.filter)
  },
  removeFilter(state, { filter }) {
    filter === 'filterName' && (state.filter.name = '')
    filter === 'filterMin' && (state.filter.min = 0)
    filter === 'filterMax' && (state.filter.max = 20)
    saveToSessionStorage('filters', state.filter)
  }
}

export const getters = {
  limits: state => state.limits,
  counters: (state) => {
    let counters = [...state.counters]
    const order = state.order.asc ? [-1, 1] : [1, -1]
    if (state.order.by === 'name') {
      counters = [
        ...counters.sort(function (a, b) {
          if (a.name < b.name) {
            return order[0]
          }
          if (a.name > b.name) {
            return order[1]
          }
          return 0
        }),
      ]
    } else if(state.order.by === 'value') {
      counters = [
        ...counters.sort(function (a, b) {
          if (state.order.asc) {
            return  b.value - a.value
          } else{
            return  a.value - b.value
          }
        }),
      ]
    }
    if (state.filter.name) {
      counters = [
        ...counters.filter((counter) => counter.name.includes(state.filter.name)),
      ]
    }
    if (state.filter.min) {
      counters = [
        ...counters.filter((counter) => counter.value >= state.filter.min),
      ]
    }
    if (state.filter.max) {
      counters = [
        ...counters.filter((counter) => counter.value <= state.filter.max),
      ]
    }
    return counters
  },
  getFilters: (state) => {
    return {
      filterName: state.filter.name,
      filterMin: state.filter.min,
      filterMax: state.filter.max,
    }
  },
  counterQty: (state) => state.counters.length,
  counterValueTotals: (state) =>
    state.counters.reduce((acc, curr) => acc + curr.value, 0),
}
