export const state = () => ({
  isVisible: false,
  component: ''
})

export const mutations = {
  showModal(state, { component }) {
    state.component = component
    state.isVisible = true
  },  
  hideModal(state) {
    state.isVisible = false
  }
}

export const getters = {
  isVisible: state => state.isVisible,
  modalComponent: state => state.component
}
