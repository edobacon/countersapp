export function saveToLocalStorage(key, value) {
  localStorage.setItem(key, JSON.stringify(value))
}

export function saveToSessionStorage(key, value) {
  sessionStorage.setItem(key, JSON.stringify(value))
}